/*
    Based on download.js v4.2, by dandavis; 2008-2016. [CCBY2] https://github.com/rndme/download
 */

export class DownloadService {
    private static defaultMime = "application/octet-stream";

    /**
     * This download script will only work for web browsers
     * @param data
     * @param strFileName
     * @param strMimeType
     */
    public static download(data, strFileName, strMimeType) {

        let mimeType = strMimeType || DownloadService.defaultMime;
        let payload = data;
        let url = !strFileName && !strMimeType && payload;
        let anchor = document.createElement("a");

        let fileName = strFileName || "download";
        let blob;
        let reader;

        if (String(this) === "true") { //reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
            payload = [payload, mimeType];
            mimeType = payload[0];
            payload = payload[1];
        }


        if (url && url.length < 2048) { // if no filename and no mime, assume a url was passed as the only argument
            fileName = url.split("/").pop().split("?")[0];
            anchor.href = url; // assign href prop to temp anchor
            if (anchor.href.indexOf(url) !== -1) { // if the browser determines that it's a potentially valid url path:
                var ajax = new XMLHttpRequest();
                ajax.open("GET", url, true);
                ajax.responseType = 'blob';
                ajax.onload = function (e: any) {
                    DownloadService.download(e.target.response, fileName, DownloadService.defaultMime);
                };
                setTimeout(function () {
                    ajax.send();
                }, 0); // allows setting custom ajax headers using the return:
                return ajax;
            } // end if valid url?
        } // end if url?


        //go ahead and download dataURLs right away
        if (/^data:([\w+-]+\/[\w+.-]+)?[,;]/.test(payload)) {
            if (payload.length > (1024 * 1024 * 1.999)) {
                payload = DownloadService.dataUrlToBlob(payload);
                mimeType = payload.type || DownloadService.defaultMime;
            } else {
                return DownloadService.saver(payload, fileName, anchor);
            }
        }

        blob = new Blob([payload], { type: mimeType });

        if (self.URL) { // simple fast and modern way using Blob and URL:
            DownloadService.saver(self.URL.createObjectURL(blob), fileName, anchor, true);
        } else {
            // handle non-Blob()+non-URL browsers:
            /*if (typeof blob === "string" || blob.constructor === toString) {
                try {
                    return DownloadService.saver("data:" + mimeType + ";base64," + self.btoa(blob), fileName, anchor);
                } catch (y) {
                    return DownloadService.saver("data:" + mimeType + "," + encodeURIComponent(blob), fileName, anchor);
                }
            }*/

            // Blob but not URL support:
            reader = new FileReader();
            reader.onload = function (e) {
                DownloadService.saver(this.result, fileName, anchor);
            };
            reader.readAsDataURL(blob);
        }
        return true;
    };

    private static dataUrlToBlob(strUrl: string) {
        var parts = strUrl.split(/[:;,]/),
            type = parts[1],
            decoder = parts[2] == "base64" ? atob : decodeURIComponent,
            binData = decoder(parts.pop()),
            mx = binData.length,
            i = 0,
            uiArr = new Uint8Array(mx);

        for (i; i < mx; ++i) uiArr[i] = binData.charCodeAt(i);

        return new Blob([uiArr], { type: type });
    }

    private static saver(url: string, fileName: string, anchor: HTMLAnchorElement, winMode: boolean = false) {
        if ('download' in anchor) { //html5 A[download]
            var element = document.createElement('a');

            element.setAttribute('href', url);
            element.setAttribute('download', fileName);
            element.style.display = 'none';

            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
            if (winMode === true) {
                setTimeout(function () {
                    self.URL.revokeObjectURL(element.href);
                }, 250);
            }

            return true;
        }

        // handle non-a[download] safari as best we can:
        if (/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
            url = url.replace(/^data:([\w\/\-\+\.]+)/, DownloadService.defaultMime);
            if (!window.open(url)) { // popup blocked, offer direct download:
                if (confirm("Displaying New Document\n\nUse Save As... to download, then click back to return to this page.")) {
                    location.href = url;
                }
            }
            return true;
        }

        //do iframe dataURL download (old ch+FF):
        var f = document.createElement("iframe");
        document.body.appendChild(f);

        if (!winMode) { // force a mime that will download:
            url = "data:" + url.replace(/^data:([\w\/\-\+\.]+)/, DownloadService.defaultMime);
        }
        f.src = url;
        setTimeout(function () {
            document.body.removeChild(f);
        }, 333);
    }
}
