export class SharingACL {
    constructor(private permission: number) {
    }

    public static readonly permissions = {
        READ: 0x01,
        WRITE: 0x02,
        FILES: 0x04,
        HISTORY: 0x08,
        OWNER: 0x80,
    };

    /**
     * Checks if a user has the given permission/s
     * @param permission
     */
    public hasPermission(permission): boolean {
        return permission === (this.permission & permission);
    };

    /**
     * Adds a permission to a user, leaving any other permissions intact
     * @param permission
     */
    public addPermission(permission) {
        this.permission = this.permission | permission;
    };

    /**
     * Removes a given permission from the item, leaving any other intact
     * @param permission
     */
    public removePermission(permission) {
        this.permission = this.permission & ~permission;
    };

    public togglePermission(permission) {
        this.permission ^= permission;
    };

    public getAccessLevel() {
        return this.permission;
    }
}
