import { pki } from "node-forge";

export interface RSAKeypairInterface {
    privateKey: pki.PrivateKey;
    publicKey: pki.PublicKey;
}
