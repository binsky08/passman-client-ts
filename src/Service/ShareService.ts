import { NextcloudServerInterface } from "../Interfaces/NextcloudServer/NextcloudServerInterface";
import Credential from "../Model/Credential";
import Vault from "../Model/Vault";
import { PassmanCrypto } from "./PassmanCrypto";
import { SharingACL } from "../Model/SharingACL";
import { ACLInterface } from "../Interfaces/ShareService/ACLInterface";
import { CredentialShareRequestInterface } from "../Interfaces/ShareService/CredentialShareRequestInterface";

export class ShareService {
    public static async getCredentialsSharedWithUs(vault: Vault, server: NextcloudServerInterface, getCachedIfPossible: boolean = false) {
        let sharedItemsResponse = await server.getJson<CredentialShareRequestInterface[]>('/sharing/vault/' + vault.guid + '/get', (response) => {
            server.logger.onError(response.message);
        }, getCachedIfPossible);

        const credentials: Credential[] = [];
        if (sharedItemsResponse) {
            for (const sharedItem of sharedItemsResponse) {
                try {
                    const decrypted_shared_key = PassmanCrypto.decryptString(sharedItem.shared_key, vault.vaultKey);
                    const sharedCredential = await Credential.fromData(sharedItem.credential_data, vault, server);
                    sharedCredential.sharedCredentialEncryptionKey = decrypted_shared_key;

                    // transfer acl data from sharedItem into the new Credential object
                    delete sharedItem.credential_data;
                    const permissionsSharingAcl = new SharingACL(sharedItem.permissions);
                    const newAcl = <ACLInterface>(sharedItem as unknown);
                    newAcl.permissions = permissionsSharingAcl;
                    sharedCredential.acl = newAcl;

                    credentials.push(sharedCredential);
                } catch (e) {
                    server.logger.anyError(e);
                    server.logger.onError('Failed to decrypt shared credential: ' + sharedItem.credential_data.label);
                }
            }
        }
        return credentials;
    }

    /**
     * Response array entries of ICredentialShareRequest does not contain the credential_data field.
     *
     * @param vault
     * @param server
     */
    public static getACLsForCredentialsSharedWithUs(vault: Vault, server: NextcloudServerInterface) {
        return server.getJson<CredentialShareRequestInterface[]>('/sharing/vault/' + vault.guid + '/acl', (response) => {
            server.logger.onError(response.message);
        });
    }

    public static updateSharedKeyForCredentialSharedWithUsByGuid(encryptedSharedKey: string, credentialGUID: string, server: NextcloudServerInterface) {
        return server.postJson<object>(
            '/sharing/credential/' + credentialGUID + '/acl/shared_key',
            {
                shared_key: encryptedSharedKey
            },
            () => {
            },
            'PATCH'
        );
    }
}
