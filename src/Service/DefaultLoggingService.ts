import type { LoggingHandlerInterface } from "../Interfaces/LoggingHandlerInterface";

export class DefaultLoggingService implements LoggingHandlerInterface {
    onDebug(message: string): void {
        console.debug(message);
    }

    onInfo(message: string): void {
        console.info(message);
    }

    onSuccess(message: string): void {
        console.log(message);
    }

    onWarning(message: string): void {
        console.warn(message);
    }

    onError(message: string): void {
        console.error(message);
    }

    anyError(error: any): void {
        console.error(error);
    }

    onThrow(error: Error): void {
        throw error;
    }
}
