import { NextcloudServerInterface } from "../Interfaces/NextcloudServer/NextcloudServerInterface";
import type Credential from "./Credential";
import { FileInterface } from "../Interfaces/File/FileInterface";
import { FileUploadResponseInterface } from "../Interfaces/File/FileUploadResponseInterface";
import { DeleteFilesRequestBodyInterface } from "../Interfaces/File/DeleteFilesRequestBodyInterface";
import { DeleteFilesResponseInterface } from "../Interfaces/File/DeleteFilesResponseInterface";
import { FileDownloadResponseInterface } from "../Interfaces/File/FileDownloadResponseInterface";

export class File {
    public static downloadFile = async (file: FileInterface, server: NextcloudServerInterface) => {
        return server.getJson<FileDownloadResponseInterface>('file/' + file.file_id, (error) => {
            server.logger.onError(error.message);
        });
    }

    public static downloadSharedFile = async (credential: Credential, file: FileInterface, server: NextcloudServerInterface) => {
        return server.getJson<FileDownloadResponseInterface>('sharing/credential/' + credential.guid + '/file/' + file.guid, (error) => {
            server.logger.onError(error.message);
        });
    }

    public static uploadFile = (fileWithEncryptedFields: FileInterface, server: NextcloudServerInterface): Promise<void | FileUploadResponseInterface> => {
        return server.postJson<FileUploadResponseInterface>(
            'file',
            fileWithEncryptedFields,
            () => {
            }
        );
    }

    public static uploadSharedFile = (credential: Credential, fileWithEncryptedFields: FileInterface, server: NextcloudServerInterface): Promise<void | FileUploadResponseInterface> => {
        return server.postJson<FileUploadResponseInterface>(
            'sharing/credential/' + credential.guid + '/file',
            fileWithEncryptedFields,
            () => {
            }
        );
    }

    public static updateFile = (fileWithEncryptedFields: FileInterface, server: NextcloudServerInterface): Promise<void | FileUploadResponseInterface> => {
        return server.postJson<FileUploadResponseInterface>(
            'file',
            fileWithEncryptedFields,
            () => {
            },
            'PATCH'
        );
    }

    public static deleteFile = (file: FileInterface, server: NextcloudServerInterface): Promise<void | FileUploadResponseInterface> => {
        return server.deleteJson<FileUploadResponseInterface>('file/' + file.file_id, (error) => {
            server.logger.onError(error.message);
        });
    }

    public static deleteFiles = (deleteFilesRequestBody: DeleteFilesRequestBodyInterface, server: NextcloudServerInterface): Promise<void | DeleteFilesResponseInterface> => {
        return server.postJson<DeleteFilesResponseInterface>(
            'files/delete',
            deleteFilesRequestBody,
            () => {
            }
        );
    }
}
