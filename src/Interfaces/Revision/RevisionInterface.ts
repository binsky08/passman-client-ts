import { EncryptedCredentialInterface } from "../Credential/EncryptedCredentialInterface";

export interface RevisionInterface {
    revision_id: number,
    guid: string,
    created: number,
    credential_data: EncryptedCredentialInterface,
    edited_by: string
}
