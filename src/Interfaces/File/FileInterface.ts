export interface FileInterface {
    filename: string,
    size: number,
    mimetype: string,
    data: string,
    file_id?: number,
    guid?: string,
    created?: number,
}
