export interface IFormFieldError {
    error: string;
    field?: string;
}
