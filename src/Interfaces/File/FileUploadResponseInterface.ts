export interface FileUploadResponseInterface {
    file_id: number,
    filename: string,   // encrypted field
    guid: string,
    size: number,
    file_data: string,  // encrypted field
    created: number,
    mimetype: string,
}
