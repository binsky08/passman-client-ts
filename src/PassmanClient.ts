import { NextcloudServer } from "./Model/NextcloudServer";
import Vault from "./Model/Vault";
import type { LoggingHandlerInterface } from "./Interfaces/LoggingHandlerInterface";
import { DefaultLoggingService } from "./Service/DefaultLoggingService";
import { NextcloudServerInfoInterface } from "./Interfaces/NextcloudServer/NextcloudServerInfoInterface";
import { VaultInterface } from "./Interfaces/Vault/VaultInterface";
import type { NextcloudServerInterface } from "./Interfaces/NextcloudServer/NextcloudServerInterface";

export class PassmanClient {
    public readonly server: NextcloudServerInterface;
    private logger: LoggingHandlerInterface;
    public vaults: Vault[];

    /**
     * Create PassmanClient instance.
     * @param serverData
     * @param nextcloudServer
     * @param logger
     * @throws ConfigurationError from nextcloud server configuration data
     */
    constructor(serverData: NextcloudServerInfoInterface, nextcloudServer?: NextcloudServerInterface, logger?: LoggingHandlerInterface) {
        this.logger = logger ?? new DefaultLoggingService();
        this.server = nextcloudServer ?? new NextcloudServer(serverData, this.logger);
    }

    /**
     * Refresh local (internal) vaults cache.
     * @param throwError
     * @param preserveInMemoryVaultKeys
     * @param getCachedIfPossible
     * @throws Error if throwError = true and the api request fails with an error
     */
    public async refreshVaults(throwError = false, preserveInMemoryVaultKeys = true, getCachedIfPossible = false): Promise<boolean> {
        let newVaults = [];
        const vaults = await this.server.getJson<VaultInterface[]>('/vaults', (error) => {
            console.error(error);
            if (throwError) {
                this.logger.onThrow(error);
            }
        }, getCachedIfPossible);
        if (vaults) {
            vaults.forEach((vaultData: VaultInterface) => {
                const vault = new Vault(vaultData, this.server);
                if (this.vaults !== undefined && preserveInMemoryVaultKeys) {
                    const oldVaultInstance = this._getVaultByGuid(vault.guid);
                    if (oldVaultInstance && oldVaultInstance.vaultKey && vault.testVaultKey(oldVaultInstance.vaultKey)) {
                        vault.vaultKey = oldVaultInstance.vaultKey;
                    }
                }
                newVaults.push(vault);
            });
            this.vaults = newVaults;
            return true;
        }
        return false;
    }

    public async createVault(vaultName: string, vaultPassword: string): Promise<void | Vault> {
        let newVault = await Vault.create(vaultName, vaultPassword, this.server);
        if (newVault) {
            this.vaults.push(newVault);
            return newVault;
        }
    }

    public async getVaultByGuid(guid: string, getCachedIfPossible = false) {
        if (this.vaults === undefined) {
            if (!await this.refreshVaults(false, true, getCachedIfPossible)) {
                this.logger.onError("failed to refresh vaults");
                return;
            }
        }
        const foundVault = this._getVaultByGuid(guid);
        if (foundVault) {
            return foundVault;
        }
        this.logger.onError(`vault with guid ${guid} not found`);
    }

    /**
     * Make sure that this.vaults is an array and not undefined, before using this method.
     * @param guid
     * @private
     */
    private _getVaultByGuid(guid: string) {
        for (const element of this.vaults) {
            if (element.guid === guid) {
                return element;
            }
        }
    }

    public async getTranslation(lang: string = 'en') {
        return await this.server.getJson<object>('/language?lang=' + lang, (response) => {
            this.logger.onError(response.message);
        });
    }
}
