export class CustomMathsService {

    public static round = (num: any, digits: number) => {
        return parseFloat(num).toFixed(digits);
    }

    public static calculateFromByte = (byte: any, roundDigits: number = 2) => {
        byte = parseFloat(byte);
        let result = '';

        if (byte < 1024) {
            result = this.round(byte, roundDigits) + ' Byte';
        } else if (byte >= 1024 && byte < Math.pow(1024, 2)) {
            result = this.round(byte / 1024, roundDigits) + ' KB';
        } else if (byte >= Math.pow(1024, 2) && byte < Math.pow(1024, 3)) {
            result = this.round(byte / Math.pow(1024, 2), roundDigits) + ' MB';
        } else if (byte >= Math.pow(1024, 3) && byte < Math.pow(1024, 4)) {
            result = this.round(byte / Math.pow(1024, 3), roundDigits) + ' GB';
        } else if (byte >= Math.pow(1024, 4) && byte < Math.pow(1024, 5)) {
            result = this.round(byte / Math.pow(1024, 4), roundDigits) + ' TB';
        } else if (byte >= Math.pow(1024, 5) && byte < Math.pow(1024, 6)) {
            result = this.round(byte / Math.pow(1024, 5), roundDigits) + ' PB';
        } else if (byte >= Math.pow(1024, 6) && byte < Math.pow(1024, 7)) {
            result = this.round(byte / Math.pow(1024, 6), roundDigits) + ' EB';
        }
        return result;
    }

    public static byteToGb = (byte: any, roundDigits: number = 2) => {
        return this.round(byte / Math.pow(1024, 3), roundDigits);
    }

    public static byteToMb = (byte: any, roundDigits: number = 2) => {
        return this.round(byte / Math.pow(1024, 2), roundDigits);
    }

    /**
     * Returns a securely generated random int between the given borders.
     * Maximum supported (and enforced) range between min and max is 256 (0...255).
     * @param min exclusive lower border
     * @param max exclusive upper border
     */
    public static getSecureRandomInt = (min: number, max: number): number => {
        // Create byte array and fill with 1 random number
        const byteArray = new Uint8Array(1);

        const max_range = 256;
        const range = Math.min(max - min + 1, max_range);

        let randomNumber: number;

        do {
            crypto.getRandomValues(byteArray);
            randomNumber = byteArray[0];
        } while (randomNumber >= Math.floor(max_range / range) * range)

        return min + (randomNumber % range);
    }
}
