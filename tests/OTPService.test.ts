import { OTPAlgorithmOptions } from "../lib/Interfaces/Credential/OTPConfigInterface";

describe('testing OTPAlgorithmOptions generation', () => {
    test('test equals', () => {
        expect(OTPAlgorithmOptions).toStrictEqual({
            "SHA1": "SHA1",
            "SHA256": "SHA256",
            "SHA512": "SHA512",
        });
    });
});
