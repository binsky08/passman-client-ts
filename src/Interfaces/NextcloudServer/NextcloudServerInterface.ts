import { LoggingHandlerInterface } from "../LoggingHandlerInterface";

export interface NextcloudServerInterface {
    logger: LoggingHandlerInterface,

    getBaseUrl(): string,

    setBaseUrl(value: string): string,

    getUser(): string,

    setUser(value: string): string,

    getToken(): string,

    setToken(value: string): string,

    getApiUrl(): string,

    getJson<T>(endpoint: string, errorCallback: (response: Error) => void, getCachedIfPossible?: boolean): Promise<T | void>,

    deleteJson<T>(endpoint: string, errorCallback: (response: Error) => void): Promise<T | void>,

    postJson<T>(endpoint: string, data: [] | object | null, errorCallback: (response: Error) => void, method?: string): Promise<T | void>
}
