import { CustomMathsService } from "../lib/Service/CustomMathsService";

describe('testing round', () => {
    test('1.23456 should result in 1.235 with 3 digits', () => {
        expect(CustomMathsService.round(1.23456, 3)).toBe("1.235");
    });
    test('1.2 should result in 1.200 with 3 digits ', () => {
        expect(CustomMathsService.round(1.2, 3)).toBe("1.200");
    });
});

describe('testing getSecureRandomInt', () => {
    test('test exact range', () => {
        const randomInt = CustomMathsService.getSecureRandomInt(0, 255);
        expect(randomInt).toBeGreaterThanOrEqual(0);
        expect(randomInt).toBeLessThanOrEqual(255);
    });
    test('test exceeding range do not break things', () => {
        const randomInt = CustomMathsService.getSecureRandomInt(-99999, 99999);
        expect(randomInt).toBeGreaterThanOrEqual(-99999);
        expect(randomInt).toBeLessThanOrEqual(99999);
    });
    test('test min range', () => {
        const testWithZeroRangeNumber = 5;
        const randomInt = CustomMathsService.getSecureRandomInt(testWithZeroRangeNumber, testWithZeroRangeNumber);
        expect(randomInt).toBeGreaterThanOrEqual(testWithZeroRangeNumber);
        expect(randomInt).toBeLessThanOrEqual(testWithZeroRangeNumber);
    });
});
