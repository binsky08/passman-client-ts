import { EncryptedCredentialInterface } from "../Credential/EncryptedCredentialInterface";

export interface VaultResponseInterface {
    vault_id?: number,
    guid?: string,
    name: string,
    created: number,
    public_sharing_key: string,
    private_sharing_key: string,
    sharing_keys_generated: number,
    last_access: number,
    challenge_password: string,
    delete_request_pending: boolean,
    vault_settings: null,
    credentials: EncryptedCredentialInterface[]
}
