import ConfigurationError from "../Exception/ConfigurationError";
import type { LoggingHandlerInterface } from "../Interfaces/LoggingHandlerInterface";
import type { NextcloudServerInfoInterface } from "../Interfaces/NextcloudServer/NextcloudServerInfoInterface";
import type { NextcloudServerInterface } from "../Interfaces/NextcloudServer/NextcloudServerInterface";
import { RequestCachingHandlerInterface } from "../Interfaces/RequestCachingHandlerInterface";

export class NextcloudServer implements NextcloudServerInterface {

    /**
     * Create NextcloudServer instance.
     * @param serverData
     * @param logger
     * @param cache
     * @throws ConfigurationError
     */
    constructor(
        private serverData: NextcloudServerInfoInterface,
        public logger: LoggingHandlerInterface,
        public cache?: RequestCachingHandlerInterface
    ) {
        if (!serverData.baseUrl.startsWith('https://') && !serverData.baseUrl.startsWith('http://')) {
            this.logger.onThrow(new ConfigurationError('Base URL (or protocol) is invalid'));
        }
        if (serverData.token.length < 3) {
            this.logger.onThrow(new ConfigurationError('Password or token is invalid'));
        }
    }

    getBaseUrl(): string {
        return this.serverData.baseUrl;
    }

    setBaseUrl(value: string) {
        if (!value.startsWith('https://')) {
            this.logger.onThrow(new ConfigurationError('Base URL is invalid'));
        }

        return this.serverData.baseUrl = value;
    }

    getUser(): string {
        return this.serverData.user;
    }

    setUser(value: string) {
        return this.serverData.user = value;
    }

    getToken(): string {
        return this.serverData.token;
    }

    setToken(value: string) {
        return this.serverData.token = value;
    }

    getApiUrl(): string {
        return `${this.getBaseUrl()}/index.php/apps/passman/api/v2/`;
    }

    protected getEncodedLogin(): string {
        return btoa(this.getUser() + ":" + this.getToken());
    }

    getJson = async <T>(endpoint: string, errorCallback: (response: Error) => void, getCachedIfPossible: boolean = false): Promise<T | void> => {
        const cachePrefix = 'cache-getJson-';
        if (getCachedIfPossible && this.cache) {
            const cachedValue = await this.cache.get(cachePrefix + endpoint)
            if (cachedValue && cachedValue !== '') {
                try {
                    return JSON.parse(cachedValue) as T;
                } catch (_) {
                    // ignore all exceptions, just continue with the non-cached request logic
                }
            }
        }

        const res = await fetch(this.getApiUrl() + endpoint, {
            headers: {
                Accept: 'application/json',
                Authorization: `Basic ${this.getEncodedLogin()}`
            },
            credentials: 'omit'
        }).catch((err: Error) => errorCallback(err));
        if (!res) {
            return;
        }
        if (res.status >= 400) {
            const data = await res.json();
            this.logger.onError(data.message);
            return;
        }

        const jsonResponse = await res.json();
        if (this.cache) {
            await this.cache.set(cachePrefix + endpoint, JSON.stringify(jsonResponse));
        }
        return (jsonResponse) as T;
    };

    deleteJson = async <T>(endpoint: string, errorCallback: (response: Error) => void): Promise<T | void> => {
        const res = await fetch(this.getApiUrl() + endpoint, {
            method: 'DELETE',
            headers: {
                Authorization: `Basic ${this.getEncodedLogin()}`
            },
            credentials: 'omit'
        }).catch((err: Error) => errorCallback(err));
        if (!res) {
            return;
        }
        if (res.status >= 400) {
            const data = await res.json();
            this.logger.onError(data.message);
            return;
        }
        return (await res.json()) as T;
    };

    /**
     * Do a post request.
     *
     * @param endpoint
     * @param data will be converted to a json string
     * @param errorCallback
     * @param method
     */
    postJson = async <T>(endpoint: string, data: [] | object | null, errorCallback: (response: Error) => void, method: string = 'POST'): Promise<T | void> => {
        const res = await fetch(this.getApiUrl() + endpoint, {
            method: method,
            headers: {
                Accept: 'application/json',
                Authorization: `Basic ${this.getEncodedLogin()}`,
                "Content-Type": "application/json",
            },
            credentials: 'omit',
            body: JSON.stringify(data),
        }).catch((err: Error) => errorCallback(err));
        if (!res) {
            return;
        }
        if (res.status >= 400) {
            const data = await res.json();
            this.logger.onError(data.message);
            return;
        }
        return (await res.json()) as T;
    };
}
