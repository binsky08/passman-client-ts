export interface FilterStatsInterface {
    allVisible: number,     // all unfiltered credentials, except deleted
    compromised: number,
    strengthLow: number,
    strengthMedium: number,
    strengthGood: number,
    expired: number,
    deleted: number,
    encryptionBroken: number
}
