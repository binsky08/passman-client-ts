import { Secret } from "otpauth";

export interface OTPConfigInterface {
    issuer?: string | undefined;
    label?: string | undefined;
    secret?: string | Secret | undefined;
    algorithm?: OTPAlgorithm | undefined;
    digits?: number | undefined;
    period?: number | undefined;
    qr_uri?: undefined | {
        qrData: string,
        image: any
    };
    type?: string;
}

export const OTPAlgorithms = ['SHA1', 'SHA256', 'SHA512'] as const;
export const OTPAlgorithmOptions = OTPAlgorithms.reduce(
    (acc, item) => {
        acc[item] = item;
        return acc;
    },
    {} as { [key: string]: string }
);
export type OTPAlgorithm = (typeof OTPAlgorithms)[number];
