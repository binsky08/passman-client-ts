import { NextcloudServerInterface } from "../Interfaces/NextcloudServer/NextcloudServerInterface";
import Vault from "./Vault";
import { PassmanCrypto } from "../Service/PassmanCrypto";
import { DownloadService } from "../Service/DownloadService";
import { File } from "./File";
import { escape } from 'html-escaper';
import { SharingACL } from "./SharingACL";
import { CredentialInterface } from "../Interfaces/Credential/CredentialInterface";
import { EncryptedCredentialInterface } from "../Interfaces/Credential/EncryptedCredentialInterface";
import { CustomFieldInterface } from "../Interfaces/Credential/CustomFieldInterface";
import { TagInterface } from "../Interfaces/Credential/TagInterface";
import { IconInterface } from "../Interfaces/Credential/IconInterface";
import { FileInterface } from "../Interfaces/File/FileInterface";
import { FileUploadResponseInterface } from "../Interfaces/File/FileUploadResponseInterface";
import { RevisionInterface } from "../Interfaces/Revision/RevisionInterface";
import { OTPConfigInterface } from "../Interfaces/Credential/OTPConfigInterface";
import { ACLInterface } from "../Interfaces/ShareService/ACLInterface";
import { FileDownloadResponseInterface } from "../Interfaces/File/FileDownloadResponseInterface";

export default class Credential {
    public ENCRYPTED_FIELDS = ['description', 'username', 'password', 'files', 'custom_fields', 'otp', 'email', 'tags', 'url', 'compromised', 'shared_key'];

    protected encryptedData: EncryptedCredentialInterface;
    private decryptedDataCache: CredentialInterface;
    public sharedCredentialEncryptionKey: string;   // this is set if the credential is shared with us (injected by ShareService)
    private foundUnspecifiedEncryptionError: boolean = false;

    // can be used to re-encrypt credential data, if the original vault key is not changed yet
    public overwriteVaultKey: string | undefined = undefined;

    constructor(protected vault: Vault, protected server: NextcloudServerInterface, encryptedData: EncryptedCredentialInterface = undefined) {
        this.encryptedData = encryptedData;
        this.decryptedDataCache = {} as CredentialInterface;
        if (encryptedData === undefined) {
            this.encryptedData = {} as EncryptedCredentialInterface;
            this.initializeAllFields();
        }
        this.vault_id = vault.vaultId;
    }

    private initializeAllFields() {
        this.user_id = null;
        this.vault_id = null;
        this.label = null;
        this.description = null;
        this.tags = [];
        this.email = null;
        this.username = null;
        this.password = null;
        this.url = null;
        this.files = [];
        this.custom_fields = [];
        this.otp = {};
        this.compromised = null;
        this.shared_key = null;
        this.favicon = null;
        this.icon = null;
        this.renew_interval = null;
        this.expire_time = 0;
        this.delete_time = 0;
        this.hidden = null;
        this.created = null;
        this.changed = null;
        this.set_share_key = null;
        this.skip_revision = null;
    }

    /**
     * Save new credential on the server.
     * The current credential object will be updated with the server response data if possible.
     */
    public async save() {
        let credentialResponse = await this.server.postJson<EncryptedCredentialInterface>('/credentials', this.encryptedData, (response) => {
            this.server.logger.onError(response.message);
        });
        if (credentialResponse) {
            this.encryptedData = credentialResponse;
            this.decryptedDataCache = {} as CredentialInterface;
            this.tags.forEach(value => value.text && this.vault.collectedTags.add(value.text));
        }
        return credentialResponse;
    }

    /**
     * Update / edit an existing credential on the server.
     * The current credential object will be updated with the server response data if possible.
     */
    public async update() {
        if (this.acl === undefined || this.acl.permissions.hasPermission(SharingACL.permissions.WRITE)) {
            let credentialResponse = await this.server.postJson<EncryptedCredentialInterface>('/credentials/' + this.guid, this.encryptedData, (response) => {
                this.server.logger.onError(response.message);
            }, 'PATCH');
            if (credentialResponse) {
                if (this.encryptedData.acl) {
                    credentialResponse.acl = this.encryptedData.acl;
                }
                this.encryptedData = credentialResponse;
                this.decryptedDataCache = {} as CredentialInterface;
                this.tags.forEach(value => value.text && this.vault.collectedTags.add(value.text));
            }
            return credentialResponse;
        }
    }

    /**
     * Refresh the local credential data based on the server, using the credentials guid.
     * It is not supported to do that for credentials, shared with us.
     */
    public async refresh() {
        if (this.sharedCredentialEncryptionKey) {
            // credential is shared with us
            // no credential refresh possible, since there is no backend api to do that
        } else {
            let credentialResponse = await this.server.getJson<EncryptedCredentialInterface>('/credentials/' + this.guid, (response) => {
                this.server.logger.onError(response.message);
            });
            if (credentialResponse) {
                this.encryptedData = credentialResponse;
                this.decryptedDataCache = {} as CredentialInterface;
                this.tags.forEach(value => value.text && this.vault.collectedTags.add(value.text));
            }
            return credentialResponse;
        }
    }

    /**
     * Destroys the credential on the server and removes itself from its local vault.
     */
    public async destroy() {
        if (this.acl === undefined || this.acl.permissions.hasPermission(SharingACL.permissions.WRITE)) {
            let credentialResponse = await this.server.deleteJson<EncryptedCredentialInterface>('/credentials/' + this.guid, (response) => {
                this.server.logger.onError(response.message);
            });
            const pos = this.vault.credentials.indexOf(this);
            this.vault.credentials.splice(pos, 1)
            return credentialResponse;
        }
    }

    public clearDecryptedDataCache() {
        this.decryptedDataCache = {} as CredentialInterface;
    }

    /**
     * Create a credential object based on its encrypted data.
     * @param data
     * @param vault
     * @param server
     */
    public static async fromData(data: EncryptedCredentialInterface, vault: Vault, server: NextcloudServerInterface) {
        return new Credential(vault, server, data);
    }

    /**
     * Create a credential object based on its guid. This will fetch the current credential data from the server.
     * @param guid
     * @param vault
     * @param server
     */
    public static async fromGuid(guid: string, vault: Vault, server: NextcloudServerInterface) {
        let cred = new Credential(vault, server);
        cred.guid = guid;
        await cred.refresh();
        return cred;
    }

    public async getRevisions() {
        return await this.server.getJson<RevisionInterface[]>('/credentials/' + this.guid + '/revision', (response) => {
            this.server.logger.onError(response.message);
        });
    }

    /**
     * Update credential (encryptedData store) with the new values, if they have changed.
     * This does not update the servers credential instance.
     * @param decryptedCredentialData
     */
    public updateData(decryptedCredentialData: CredentialInterface) {
        // todo: fill Interfaces folder
        Object.keys(decryptedCredentialData).forEach((key) => {
            if (
                key !== 'files' ||
                (
                    key === 'files' &&
                    this.acl === undefined ||
                    this.acl.permissions.hasPermission(SharingACL.permissions.FILES)
                )
            ) {
                if (this.ENCRYPTED_FIELDS.indexOf(key) > -1) {
                    // todo: improve check if array or object types have changed
                    if (this.encryptedData[key] !== decryptedCredentialData[key] || typeof decryptedCredentialData[key] !== 'string') {
                        if (key === 'shared_key') {
                            // this special field is not encrypted, if it only contains a null value, since it won't be json stringified
                            if (decryptedCredentialData[key] !== undefined && decryptedCredentialData[key] !== null && decryptedCredentialData[key] !== 'null' && decryptedCredentialData[key] !== '') {
                                this.encryptedData[key] = decryptedCredentialData[key];
                            }
                        } else {
                            this.setEncryptFieldData(key, decryptedCredentialData[key]);
                        }
                    }
                } else {
                    this.encryptedData[key] = decryptedCredentialData[key];
                }
            }
        });
    }

    public exportData(): CredentialInterface {
        return {
            user_id: this.user_id,
            vault_id: this.vault_id,
            label: this.label,
            description: this.description,
            tags: this.tags,
            email: this.email,
            username: this.username,
            password: this.password,
            url: this.url,
            files: (this.acl === undefined || this.acl.permissions.hasPermission(SharingACL.permissions.FILES)) ? this.files : [],
            custom_fields: this.custom_fields,
            otp: this.otp,
            compromised: this.compromised,
            shared_key: this.shared_key,
            favicon: this.favicon,
            icon: this.icon,
            renew_interval: this.renew_interval,
            expire_time: this.expire_time,
            delete_time: this.delete_time,
            hidden: this.hidden,
            created: this.created,
            changed: this.changed,
            set_share_key: this.set_share_key,
            skip_revision: this.skip_revision
        };
    }

    /**
     * Creates a local 100% clone of the current credential.
     */
    public clone(): Credential {
        const newCredential = new Credential(this.vault, this.server, this.encryptedData);
        newCredential.sharedCredentialEncryptionKey = this.sharedCredentialEncryptionKey;

        return newCredential;
    }

    /**
     * Check whether the credential has a valid shared_key.
     * If true, it is a credential, that's encrypted with the decrypted shared_key field content.
     */
    public hasValidSharedKey() {
        return this.encryptedData.shared_key !== undefined
            && this.encryptedData.shared_key !== null
            && this.encryptedData.shared_key !== 'null'
            && this.encryptedData.shared_key !== '';
    }

    /**
     * Creates a local 100% clone of the current credential and re-encrypts it with the given key.
     */
    public reEncryptAsClone(newVaultKey: string): Credential {
        const newCredential = this.clone();

        if (this.hasValidSharedKey()) {
            // only re-encrypt the shared key, if the credential is shared and not encrypted with the vault key like default credentials
            const decrypted_shared_key = newCredential.shared_key;
            newCredential.set_share_key = true;
            newCredential.skip_revision = true;
            newCredential.overwriteVaultKey = newVaultKey;
            newCredential.encryptedData.shared_key = PassmanCrypto.encryptString(decrypted_shared_key, newVaultKey)
        } else {
            // default (100% owned) credential
            const decryptedCredentialData = newCredential.exportData();
            newCredential.overwriteVaultKey = newVaultKey;
            newCredential.updateData(decryptedCredentialData);
            newCredential.skip_revision = true;
        }
        return newCredential;
    }

    private getFieldEncryptionKey(fieldName: string = null): string {
        if (
            fieldName !== 'shared_key' &&
            this.encryptedData.shared_key !== undefined &&
            this.encryptedData.shared_key !== null &&
            this.encryptedData.shared_key !== 'null' &&
            this.encryptedData.shared_key !== ''
        ) {
            // got a credential that is shared with others
            if (this.decryptedDataCache.shared_key === undefined) {
                this.decryptedDataCache.shared_key = PassmanCrypto.decryptString(this.encryptedData.shared_key, this.overwriteVaultKey ?? this.vault.vaultKey);
            }
            if (this.decryptedDataCache.shared_key != null) {
                // is this.encryptedData.shared_key is set, but a null value was encrypted, do not return it
                return this.decryptedDataCache.shared_key;
            }
        }

        if (this.sharedCredentialEncryptionKey !== undefined) {
            return this.sharedCredentialEncryptionKey;
        }

        return this.overwriteVaultKey ?? this.vault.vaultKey;
    }

    /**
     * Returns (json parsed) field data and decrypt / caches it if required.
     *
     * @param property key of ILEncryptedCredential
     * @private
     */
    private getCacheDecryptFieldData(property: string): any {
        if (this.ENCRYPTED_FIELDS.indexOf(property) > -1) {
            if (this.decryptedDataCache[property] === undefined) {
                if (this.encryptedData[property] === null) {
                    this.decryptedDataCache[property] = null;
                } else {
                    let decryptedData: string;
                    try {
                        decryptedData = PassmanCrypto.decryptString(this.encryptedData[property], this.getFieldEncryptionKey(property));
                        // check for non-json content field
                        if (property === 'shared_key') {
                            this.decryptedDataCache[property] = decryptedData;
                        } else {
                            this.decryptedDataCache[property] = JSON.parse(decryptedData);
                        }
                    } catch (e) {
                        this.foundUnspecifiedEncryptionError = true;
                        this.server.logger.anyError(e);
                        this.server.logger.anyError(decryptedData);
                        this.server.logger.onError('Failed to decrypt field: ' + property + ' of credential: ' + this.label);
                    }
                }
            }
            return this.decryptedDataCache[property];
        }

        // no need to cache nor json parse fields without encryption
        return this.encryptedData[property];
    }

    /**
     * Pass the plain field data so that it can be stored stringified and encrypted.
     *
     * @param property
     * @param data
     * @private
     */
    private setEncryptFieldData(property: string, data: any) {
        if (this.ENCRYPTED_FIELDS.indexOf(property) > -1) {
            try {
                // check for non-json content field
                if (property === 'shared_key') {
                    this.encryptedData[property] = PassmanCrypto.encryptString(data, this.getFieldEncryptionKey(property))
                } else {
                    this.encryptedData[property] = PassmanCrypto.encryptString(JSON.stringify(data), this.getFieldEncryptionKey(property))
                }
            } catch (e) {
                this.foundUnspecifiedEncryptionError = true;
                this.server.logger.anyError(e);
                this.server.logger.onError('Failed to encrypt field: ' + property + ' of credential: ' + this.label);
                return;
            }
            this.decryptedDataCache[property] = data;
        } else {
            this.encryptedData[property] = data;
        }
    }

    public async downloadFile(file: FileInterface, directDownload: boolean = true) {
        if (file.filename === undefined || file.mimetype === undefined) {
            return;
        }
        const key = this.getFieldEncryptionKey();

        const logger = this.server.logger;
        const callback = function (result: FileDownloadResponseInterface) {
            if (!result.hasOwnProperty('file_data')) {
                logger.onError('Error downloading file (' + file.filename + '), you probably have insufficient permissions');
                return;
            }

            try {
                const file_data = PassmanCrypto.decryptString(result.file_data, key)
                if (directDownload) {
                    DownloadService.download(file_data, escape(file.filename), file.mimetype);
                } else {
                    return file_data;
                }
            } catch (e) {
                logger.anyError(e);
                logger.onError('Failed to decrypt file: ' + file.filename);
            }
        };

        if (!this.encryptedData.acl) {
            return File.downloadFile(file, this.server).then(callback);
        } else {
            return File.downloadSharedFile(this, file, this.server).then(callback);
        }
    }

    /**
     * Takes plain file data, encrypts and uploads it.
     * This method does *not* add the file to the local credential files list!
     * Adding it to the files list as well as a call to save() or update() is required to associate the file persistent with the credential.
     * @param plainFile
     */
    public encryptUploadFile(plainFile: FileInterface): Promise<void | FileUploadResponseInterface> {
        try {
            const encryptedFile: FileInterface = {
                data: PassmanCrypto.encryptString(plainFile.data, this.getFieldEncryptionKey()),
                filename: PassmanCrypto.encryptString(JSON.stringify(plainFile.filename), this.getFieldEncryptionKey()),
                mimetype: plainFile.mimetype,
                size: plainFile.size
            };
            const callback = function (uploadFileResponse: void | FileUploadResponseInterface) {
                return uploadFileResponse;
            }

            if (!this.encryptedData.acl) {
                return File.uploadFile(encryptedFile, this.server).then(callback);
            } else {
                return File.uploadSharedFile(this, encryptedFile, this.server).then(callback);
            }
        } catch (e) {
            this.foundUnspecifiedEncryptionError = true;
            this.server.logger.anyError(e);
            this.server.logger.onError('Failed to encrypt new file (' + plainFile.filename + ') of credential: ' + this.label);
        }
    }

    /**
     * Deletes the given file from the server.
     * This method does *not* delete the file from the local credential files list!
     * @param file
     */
    public deleteFile(file: FileInterface): Promise<void | FileUploadResponseInterface> {
        return File.deleteFile(file, this.server);
    }

    public getVaultGuid(): string {
        return this.vault.guid;
    }

    public getEncrypted() {
        return this.encryptedData;
    }

    public hasUnspecifiedEncryptionError(): boolean {
        return this.foundUnspecifiedEncryptionError;
    }

    get credential_id(): number {
        return this.encryptedData.credential_id;
    }

    set credential_id(value: number) {
        this.encryptedData.credential_id = value;
    }

    get guid(): string {
        return this.encryptedData.guid;
    }

    set guid(value: string) {
        this.encryptedData.guid = value;
    }

    get user_id(): string {
        return this.encryptedData.user_id;
    }

    set user_id(value: string) {
        this.encryptedData.user_id = value;
    }

    get vault_id(): number {
        return this.encryptedData.vault_id;
    }

    set vault_id(value: number) {
        this.encryptedData.vault_id = value;
    }

    get label(): string {
        return this.encryptedData.label;
    }

    set label(value: string) {
        this.encryptedData.label = value;
    }

    get description(): string {
        return this.getCacheDecryptFieldData('description');
    }

    set description(value: string) {
        this.setEncryptFieldData('description', value);
    }

    get tags(): TagInterface[] {
        return this.getCacheDecryptFieldData('tags');
    }

    set tags(value: TagInterface[]) {
        this.setEncryptFieldData('tags', value);
    }

    get email(): string {
        return this.getCacheDecryptFieldData('email');
    }

    set email(value: string) {
        this.setEncryptFieldData('email', value);
    }

    get username(): string {
        return this.getCacheDecryptFieldData('username');
    }

    set username(value: string) {
        this.setEncryptFieldData('username', value);
    }

    get password(): string {
        return this.getCacheDecryptFieldData('password');
    }

    set password(value: string) {
        this.setEncryptFieldData('password', value);
    }

    get url(): string {
        return this.getCacheDecryptFieldData('url');
    }

    set url(value: string) {
        this.setEncryptFieldData('url', value);
    }

    get files(): FileInterface[] {
        return this.getCacheDecryptFieldData('files');
    }

    set files(value: FileInterface[]) {
        this.setEncryptFieldData('files', value);
    }

    get custom_fields(): CustomFieldInterface[] {
        return this.getCacheDecryptFieldData('custom_fields');
    }

    set custom_fields(value: CustomFieldInterface[]) {
        this.setEncryptFieldData('custom_fields', value);
    }

    get otp(): OTPConfigInterface {
        return this.getCacheDecryptFieldData('otp');
    }

    set otp(value: OTPConfigInterface) {
        this.setEncryptFieldData('otp', value);
    }

    get compromised(): boolean {
        return this.getCacheDecryptFieldData('compromised');
    }

    set compromised(value: boolean) {
        this.setEncryptFieldData('compromised', value);
    }

    /**
     * This special field is not encrypted, if it only contains a null value.
     */
    get shared_key(): string | null {
        if (this.encryptedData.shared_key !== undefined && this.encryptedData.shared_key !== null && this.encryptedData.shared_key !== 'null' && this.encryptedData.shared_key !== '') {
            return this.getCacheDecryptFieldData('shared_key');
        }
        return null;
    }

    /**
     * This special field is not encrypted, if it only contains a null value.
     * @param value
     */
    set shared_key(value: string | null) {
        if (value === null || value === '' || value === 'null') {
            this.encryptedData.shared_key = null;
        } else {
            this.setEncryptFieldData('shared_key', value);
        }
    }

    get favicon(): string {
        return this.encryptedData.favicon;
    }

    set favicon(value: string) {
        this.encryptedData.favicon = value;
    }

    get icon(): IconInterface | null {
        return this.encryptedData.icon;
    }

    set icon(value: IconInterface | null) {
        this.encryptedData.icon = value;
    }

    get renew_interval(): number | null {
        return this.encryptedData.renew_interval;
    }

    set renew_interval(value: number | null) {
        this.encryptedData.renew_interval = value;
    }

    get expire_time(): number {
        return this.encryptedData.expire_time;
    }

    set expire_time(value: number) {
        this.encryptedData.expire_time = value;
    }

    get delete_time(): number {
        return this.encryptedData.delete_time;
    }

    set delete_time(value: number) {
        this.encryptedData.delete_time = value;
    }

    get hidden(): boolean {
        return this.encryptedData.hidden;
    }

    set hidden(value: boolean) {
        this.encryptedData.hidden = value;
    }

    get created(): number {
        return this.encryptedData.created;
    }

    set created(value: number) {
        this.encryptedData.created = value;
    }

    get changed(): number {
        return this.encryptedData.changed;
    }

    set changed(value: number) {
        this.encryptedData.changed = value;
    }

    get set_share_key(): boolean {
        return this.encryptedData.set_share_key;
    }

    set set_share_key(value: boolean) {
        this.encryptedData.set_share_key = value;
    }

    get skip_revision(): boolean {
        return this.encryptedData.skip_revision;
    }

    set skip_revision(value: boolean) {
        this.encryptedData.skip_revision = value;
    }

    get acl(): ACLInterface {
        return this.encryptedData.acl;
    }

    set acl(value: ACLInterface) {
        this.encryptedData.acl = value;
    }
}
