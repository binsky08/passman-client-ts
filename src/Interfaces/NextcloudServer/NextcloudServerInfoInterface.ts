export interface NextcloudServerInfoInterface {
    baseUrl: string,
    user: string,
    token: string,
    persistence: string
}
