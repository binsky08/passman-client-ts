export interface VaultDeleteResponseInterface {
    ok: boolean,
    failed: []
}
