import { EncryptedCredentialInterface } from "../Credential/EncryptedCredentialInterface";

export interface CredentialShareRequestInterface {
    acl_id: number,
    item_id: number,
    item_guid: string,
    user_id: string,
    created: number,
    expire: number,
    expire_views: number,
    permissions: number,
    vault_id: number,
    vault_guid: string,
    shared_key: string,     // encrypted field
    pending: boolean,
    credential_data?: EncryptedCredentialInterface
}
