import { CustomFieldInterface } from "./CustomFieldInterface";
import { TagInterface } from "./TagInterface";
import { IconInterface } from "./IconInterface";
import { FileInterface } from "../File/FileInterface";
import { OTPConfigInterface } from "./OTPConfigInterface";

export interface CredentialInterface {
    // plain fields
    credential_id?: number,
    guid?: string,
    user_id: string,
    vault_id: number,
    label: string,

    // fields to encrypt
    description: string,
    tags: TagInterface[],
    email: string,
    username: string,
    password: string,
    url: string,
    files: FileInterface[],
    custom_fields: CustomFieldInterface[],
    otp: OTPConfigInterface,
    compromised: boolean,
    shared_key: string | null

    // plain fields
    favicon: string,        // old, does this field still exist?
    icon: IconInterface | null,
    renew_interval: number | null,
    expire_time: number,
    delete_time: number,
    hidden: boolean,
    created: number,
    changed: number,
    set_share_key: boolean | null,
    skip_revision: boolean | null
}
