import Vault from "../../lib/Model/Vault";
import { faker } from '@faker-js/faker';
import Credential from "../../lib/Model/Credential";
import { LoggingHandlerInterface } from "../../lib/Interfaces/LoggingHandlerInterface";
import { NextcloudServerInterface } from "../../lib/Interfaces/NextcloudServer/NextcloudServerInterface";

export class CredentialFakerFactory {
    public build = (vault: Vault, pushToVault = false, credentialOptions: { [key: string]: any } = {}, customLogger: LoggingHandlerInterface = undefined) => {
        let credential = new Credential(vault, {
            logger: customLogger
        } as NextcloudServerInterface);
        credential.label = faker.company.name();
        credential.password = faker.internet.password();
        credential.username = faker.internet.userName();
        credential.email = faker.internet.email();
        credential.description = faker.lorem.words({ min: 3, max: 300 });

        for (const credentialOptionKey of Object.keys(credentialOptions)) {
            credential[credentialOptionKey] = credentialOptions[credentialOptionKey];
        }

        if (pushToVault) {
            vault.credentials.push(credential);
        }

        return credential;
    }
}
