export interface LoggingHandlerInterface {
    onDebug(message: string): void;

    onInfo(message: string): void;

    onSuccess(message: string): void;

    onWarning(message: string): void;

    onError(message: string): void;

    anyError(error: any): void;

    onThrow(error: Error): void;
}
