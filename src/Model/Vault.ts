import { NextcloudServerInterface } from "../Interfaces/NextcloudServer/NextcloudServerInterface";
import Credential from "./Credential";
import { PassmanCrypto } from "../Service/PassmanCrypto";
import { ShareService } from "../Service/ShareService";
import { File } from "./File";
import { VaultInterface } from "../Interfaces/Vault/VaultInterface";
import { VaultResponseInterface } from "../Interfaces/Vault/VaultResponseInterface";
import { GenerateKeypairResponseInterface } from "../Interfaces/PassmanCrypto/GenerateKeypairResponseInterface";
import { VaultDeleteResponseInterface } from "../Interfaces/Vault/VaultDeleteResponseInterface";

export default class Vault {
    private _vaultKey: string;
    public collectedTags = new Set<string>();

    constructor(private data: VaultInterface, private server: NextcloudServerInterface) {
    }

    public static async create(vaultName: string, vaultPassword: string, server: NextcloudServerInterface): Promise<Vault> {
        let vaultResponse = await server.postJson<VaultInterface>('/vaults', { vault_name: vaultName }, (response) => {
            server.logger.onError(response.message);
        });
        if (vaultResponse) {
            const vault = new Vault(vaultResponse, server);
            vault._vaultKey = vaultPassword;

            await PassmanCrypto.generateRSAKeypair(2048).then(async (value: GenerateKeypairResponseInterface) => {
                if (value.keypair) {
                    const pemKeyPair = PassmanCrypto.rsaKeyPairToPEM(value.keypair);
                    await vault.updateSharingKeys(pemKeyPair.publicKey, pemKeyPair.privateKey);

                    // todo: create hidden test credential
                    let testCredential = new Credential(vault, server);
                    testCredential.label = 'Test key for vault ' + vaultName;
                    testCredential.hidden = true;
                    testCredential.password = 'lorum ipsum';
                    if (await testCredential.save()) {
                        // todo: refresh / log in to the new vault
                        server.logger.onSuccess('Vault successfully created');
                    } else {
                        server.logger.onError('Failed saving test credential in new vault');
                    }
                } else {
                    server.logger.onError('Failed to create a new vault rsa key-pair');
                }
            });

            return vault;
        }
    }

    public async refresh(getCachedIfPossible: boolean = false) {
        let vaultResponse = await this.server.getJson<VaultResponseInterface>('/vaults/' + this.guid, (response) => {
            this.server.logger.onError(response.message);
        }, getCachedIfPossible);
        if (vaultResponse) {
            const credentials: Credential[] = [];
            let collectedTags = [];
            for (const credentialData of vaultResponse.credentials) {
                try {
                    const credential = await Credential.fromData(credentialData, this, this.server);
                    if (credential.tags && credential.tags.length > 0) {
                        collectedTags = collectedTags.concat(credential.tags.map(value => value.text).filter(Boolean));
                    }
                    credentials.push(credential);
                } catch (e) {
                    this.server.logger.anyError(e);
                    this.server.logger.onError('Failed to decrypt credential: ' + credentialData.label);
                }
            }
            this.collectedTags = new Set<string>(collectedTags);

            const credentialsSharedWithUs = await ShareService.getCredentialsSharedWithUs(this, this.server, getCachedIfPossible);
            for (const credential of credentialsSharedWithUs) {
                credentials.push(credential);
            }

            // todo: check if a creation of vault.private_sharing_key for old vaults is required

            this.data = {
                challenge_password: vaultResponse.challenge_password,
                created: vaultResponse.created,
                credentials: credentials,
                delete_request_pending: vaultResponse.delete_request_pending,
                guid: vaultResponse.guid,
                last_access: vaultResponse.last_access,
                name: vaultResponse.name,
                private_sharing_key: vaultResponse.private_sharing_key,
                public_sharing_key: vaultResponse.public_sharing_key,
                sharing_keys_generated: vaultResponse.sharing_keys_generated,
                vault_id: vaultResponse.vault_id,
                vault_settings: vaultResponse.vault_settings,
            };
            return true;
        }
        return false;
    }

    public async update() {
        const result = await this.server.postJson<null>('/vaults/' + this.data.guid,
            {
                guid: this.data.guid,
                vault_id: this.data.vault_id,
                name: this.data.name,
                created: this.data.created,
                public_sharing_key: this.data.public_sharing_key,
                last_access: this.data.last_access,
                delete_request_pending: this.data.delete_request_pending,
                sharing_keys_generated: this.data.sharing_keys_generated,
                vault_settings: this.data.vault_settings
            }, (response) => {
                this.server.logger.onError(response.message);
            }, 'PATCH');
        return result === null;
    }

    public async delete(currentPassword: string) {
        if (!this.testVaultKey(currentPassword)) {
            this.server.logger.onError('Invalid password!');
            return false;
        }

        let fileIds = [];
        for (const credential of this.credentials) {
            for (const file of credential.files) {
                if (file.file_id) {
                    fileIds.push(file.file_id);
                }
            }
        }
        const deleteFilesResponse = await File.deleteFiles({ file_ids: JSON.stringify(fileIds) }, this.server);
        if (!deleteFilesResponse) {
            // void response should "never" happen with a working server side
            this.server.logger.onError('Abort! Failed to request files deletion.');
            return false;
        }
        if (!deleteFilesResponse.ok) {
            // minor file deletion issues; continue vault deletion
            this.server.logger.onError('Failed to delete files: ' + deleteFilesResponse.failed.toString());
        }

        let deleteVaultResponse = await this.server.deleteJson<VaultDeleteResponseInterface>('/vaults/' + this.guid, (response) => {
            this.server.logger.onError(response.message);
        });
        if (!deleteVaultResponse) {
            // void response should "never" happen with a working server side
            this.server.logger.onError('Abort! Failed to request vault deletion.');
            return false;
        }

        return deleteVaultResponse.ok;
    }

    public lock() {
        this.vaultKey = null;
        this.collectedTags.clear();
        // clear decrypted credential cache from memory
        for (const credential of this.data.credentials) {
            credential.clearDecryptedDataCache();
        }
    }

    public async updateSharingKeys(public_sharing_key: string, private_sharing_key: string) {
        return await this.server.postJson<void>('/vaults/' + this.data.guid + '/sharing-keys',
            {
                guid: this.data.guid,
                public_sharing_key: public_sharing_key,
                private_sharing_key: PassmanCrypto.encryptString(private_sharing_key, this.vaultKey),
            }, (response) => {
                this.server.logger.onError(response.message);
            });
    }

    public testVaultKey(vaultKey: string): boolean {
        try {
            if (this.challenge_password) {
                PassmanCrypto.decryptString(this.challenge_password, vaultKey);
                return true;
            } else if (this.credentials.length > 0) {
                PassmanCrypto.decryptString(this.getChallengingFieldValue(), vaultKey);
                return true;
            }
        } catch (e) {
        }
        return false;
    }

    private getChallengingFieldValue() {
        const testCredential = this.getFirstOwnedCredential();
        if (testCredential.getEncrypted().username != null) {
            return testCredential.getEncrypted().username;
        } else if (testCredential.getEncrypted().password != null) {
            return testCredential.getEncrypted().password;
        } else if (testCredential.getEncrypted().email != null) {
            return testCredential.getEncrypted().email;
        }
        return null;
    }

    private getFirstOwnedCredential(): Credential | undefined {
        for (let credential of this.credentials) {
            if (!credential.hasValidSharedKey() && credential.sharedCredentialEncryptionKey === undefined) {
                return credential;
            }
        }
    }

    public getCredentialByGuid(guid: string): Credential | undefined {
        for (let credential of this.credentials) {
            if (credential.guid === guid) {
                return credential;
            }
        }
    }

    public getServer(): NextcloudServerInterface {
        return this.server;
    }

    get vaultId(): number | null {
        return this.data.vault_id;
    }

    get vaultKey(): string {
        return this._vaultKey;
    }

    set vaultKey(value: string) {
        this._vaultKey = value;
    }

    get guid(): string {
        return this.data.guid;
    }

    get name(): string {
        return this.data.name;
    }

    set name(value: string) {
        this.data.name = value;
    }

    get created(): number {
        return this.data.created;
    }

    get public_sharing_key(): string {
        return this.data.public_sharing_key;
    }

    get private_sharing_key(): string {
        return PassmanCrypto.decryptString(this.data.private_sharing_key, this.vaultKey);
    }

    get sharing_keys_generated(): number {
        return this.data.sharing_keys_generated;
    }

    set sharing_keys_generated(value: number) {
        this.data.sharing_keys_generated = value;
    }

    get last_access(): number {
        return this.data.last_access;
    }

    get challenge_password(): string {
        return this.data.challenge_password;
    }

    get delete_request_pending(): boolean {
        return this.data.delete_request_pending;
    }

    set delete_request_pending(value: boolean) {
        this.data.delete_request_pending = value;
    }

    get vault_settings(): null {
        return this.data.vault_settings;
    }

    set vault_settings(value: null) {
        this.data.vault_settings = value;
    }

    get credentials(): Credential[] {
        return this.data.credentials ?? [];
    }
}
