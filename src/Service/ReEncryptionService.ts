import type Vault from "../Model/Vault";
import Credential from "../Model/Credential";
import { File } from "../Model/File";
import Revision from "../Model/Revision";
import { ShareService } from "./ShareService";
import { PassmanCrypto } from "./PassmanCrypto";
import { FileInterface } from "../Interfaces/File/FileInterface";
import { RevisionInterface } from "../Interfaces/Revision/RevisionInterface";
import { ReEncryptionProgressInterface } from "../Interfaces/ReEncryptionService/ReEncryptionProgressInterface";

export class ReEncryptionService {
    /**
     * This method starts a multistage vault key change process.
     * Persisting the key change requires a positive validation of all re-encryption steps.
     * This means the changed credentials will be uploaded once at the end, if no failure occurred.
     * The method returns void when it finished (successful or failed).
     *
     * 1. stage: re-encrypt and validate all credentials
     * 2. stage: re-encrypt all credential revisions
     * 3. stage: re-encrypt all files
     * 4. stage: runs only if the previous stages were successful: upload all data, update sharing keys
     *
     * @param vault
     * @param currentPassword
     * @param newPassword should be valid and not equal to currentPassword.
     * @param onProgress will be called once on stage start with doneSteps=0 and after every (successful or failed) step.
     * @param onError will be called when an error occurs, multiple times per step is possible.
     */
    public static async changeVaultKey(
        vault: Vault,
        currentPassword: string,
        newPassword: string,
        onProgress: (state: ReEncryptionProgressInterface) => void,
        onError: (nextErrorMessage: string) => void
    ) {
        if (currentPassword == newPassword || !vault.testVaultKey(currentPassword)) {
            onError('Invalid current or new password!');
            return;
        }
        if (!await vault.refresh()) {
            onError('Vault refresh before re-encryption failed!');
            return;
        }

        const progressState = {
            credentialsStage: {
                doneSteps: 0,
                totalSteps: 0,
                messages: []
            },
            revisionsStage: null,
            filesStage: null,
            uploadStage: null,
        };
        const originalCredentials = vault.credentials;
        const newCredentials: Credential[] = [];
        let foundEncryptionOrValidationFailure = false;
        let totalFilesCount = 0;

        progressState.credentialsStage.totalSteps = originalCredentials.length;
        for (let i = 0; i < originalCredentials.length; i++) {
            progressState.credentialsStage.doneSteps = i;
            if (originalCredentials[i].sharedCredentialEncryptionKey) {
                // don't re-encrypt credentials that are shared with us
                progressState.credentialsStage.messages.push('Skip shared credential: ' + originalCredentials[i].label);
                onProgress(progressState);
                continue;
            }
            progressState.credentialsStage.messages.push('Re-encrypt credential: ' + originalCredentials[i].label);
            onProgress(progressState);

            let newCredential: Credential;
            try {
                newCredential = originalCredentials[i].reEncryptAsClone(newPassword);
                newCredentials.push(newCredential);

                // count files (including custom field files) for the progress of a later stage
                totalFilesCount += newCredential.files.length;
                totalFilesCount += newCredential.custom_fields.filter((customField) => customField.field_type === 'file').length;
            } catch (e) {
                onError(originalCredentials[i].label + ": " + e.toString());
                progressState.credentialsStage.messages.push('Credential ' + originalCredentials[i].guid + ' error: ' + e.toString());
                foundEncryptionOrValidationFailure = true;
            }

            // validate new credential encryption with an all fields decryption using exportData()
            if (newCredential) {
                newCredential.exportData();
                if (newCredential.hasUnspecifiedEncryptionError()) {
                    onError(originalCredentials[i].label + ": Credential decryption validation failed!");
                    foundEncryptionOrValidationFailure = true;
                }
            }
        }
        progressState.credentialsStage.doneSteps = originalCredentials.length;
        progressState.credentialsStage.messages.push('Done');
        onProgress(progressState);

        // re-encrypt revisions
        progressState.revisionsStage = {
            doneSteps: 0,
            totalSteps: newCredentials.length,
            messages: []
        };
        const revisionsToUpdateOnSuccess: RevisionInterface[] = [];

        for (let i = 0; i < newCredentials.length; i++) {
            if (newCredentials[i].hasValidSharedKey()) {
                // don't re-encrypt revisions for credentials that we shared with others, since they are encrypted with the already re-encrypted shared_key

                progressState.revisionsStage.doneSteps = i;
                progressState.revisionsStage.messages.push('Skip shared credential revisions: ' + newCredentials[i].label);
                onProgress(progressState);
                continue;
            } else {
                progressState.revisionsStage.doneSteps = i;
                progressState.revisionsStage.messages.push('Re-encrypt credential revisions: ' + newCredentials[i].label);
                onProgress(progressState);
            }

            const revisions = await newCredentials[i].getRevisions();
            if (revisions && revisions.length > 0) {
                for (let j = 0; j < revisions.length; j++) {
                    try {
                        const tmpCredential = await Revision.fromData(revisions[j].credential_data, vault, vault.getServer());
                        revisions[j].credential_data = tmpCredential.reEncryptAsClone(newPassword).getEncrypted();
                        revisionsToUpdateOnSuccess.push(revisions[j]);
                    } catch (e) {
                        onError("Revision " + revisions[j].revision_id + ": " + e.toString());
                        progressState.revisionsStage.messages.push("Revision " + revisions[j].revision_id + " error: " + e.toString());
                        foundEncryptionOrValidationFailure = true;
                    }
                }
            }
        }
        progressState.revisionsStage.doneSteps = newCredentials.length;
        progressState.revisionsStage.messages.push('Done');
        onProgress(progressState);

        console.log("foundEncryptionOrValidationFailure: ", foundEncryptionOrValidationFailure);
        if (!foundEncryptionOrValidationFailure) {
            // re-encrypt files
            const filesToDeleteAfterwardsOnSuccess: FileInterface[] = [];
            const filesToDeleteAfterwardsOnFailure: FileInterface[] = [];
            let anyFileReEncryptionErrorOccurred = false;

            progressState.filesStage = {
                doneSteps: -1,
                totalSteps: totalFilesCount,
                messages: []
            };
            for (let i = 0; i < newCredentials.length; i++) {
                if (newCredentials[i].hasValidSharedKey()) {
                    // don't re-encrypt files for credentials that we shared with others, since they are encrypted with the already re-encrypted shared_key

                    progressState.filesStage.doneSteps += newCredentials[i].files.length;
                    progressState.filesStage.messages.push('Skip shared credential files: ' + newCredentials[i].label);
                    onProgress(progressState);
                    continue;
                }

                let iFiles = newCredentials[i].files;
                for (let j = 0; j < iFiles.length; j++) {
                    progressState.filesStage.doneSteps++;
                    progressState.filesStage.messages.push('Re-encrypt credential file: ' + iFiles[j].filename);
                    onProgress(progressState);

                    // logic to re-encrypt file and replace its credential reference at the end as well as deleting the old file from the server
                    try {
                        newCredentials[i].overwriteVaultKey = undefined;    // set undefined to use the current key to decrypt data
                        let decryptedFileContent = await newCredentials[i].downloadFile(iFiles[j], false);
                        const plainFile: FileInterface = {
                            data: decryptedFileContent,
                            filename: iFiles[j].filename,
                            mimetype: iFiles[j].mimetype,
                            size: iFiles[j].size
                        };
                        newCredentials[i].overwriteVaultKey = newPassword;

                        const uploadResponse = await newCredentials[i].encryptUploadFile(plainFile);
                        if (uploadResponse) {
                            delete plainFile.data;
                            plainFile.file_id = uploadResponse.file_id;
                            plainFile.guid = uploadResponse.guid;
                            plainFile.created = iFiles[j].created;

                            filesToDeleteAfterwardsOnFailure.push(plainFile);
                            filesToDeleteAfterwardsOnSuccess.push(iFiles[j]);
                            iFiles[j] = plainFile;
                        } else {
                            // error upload failed
                            onError('An error occurred while uploading a re-encrypted file! - ' + newCredentials[i].label + ": " + iFiles[j].filename);
                            progressState.filesStage.messages.push('File ' + iFiles[j].file_id + ' upload error');
                            anyFileReEncryptionErrorOccurred = true;
                        }
                    } catch (e) {
                        onError('An error occurred while re-encrypting a file! - ' + newCredentials[i].label + ": " + iFiles[j].filename);
                        progressState.filesStage.messages.push('File ' + iFiles[j].file_id + ' error: ' + e.toString());
                        anyFileReEncryptionErrorOccurred = true;
                    }
                }
                newCredentials[i].files = iFiles;
            }

            // re-encrypt custom field files
            for (let i = 0; i < newCredentials.length; i++) {
                let iCustomFields = newCredentials[i].custom_fields;
                for (let j = 0; j < iCustomFields.length; j++) {
                    if (iCustomFields[j].field_type !== 'file') {
                        continue;
                    }

                    const iFile = iCustomFields[j].value as FileInterface;

                    if (newCredentials[i].hasValidSharedKey()) {
                        // don't re-encrypt custom field files for credentials that we shared with others, since they are encrypted with the already re-encrypted shared_key

                        progressState.filesStage.doneSteps++;
                        progressState.filesStage.messages.push('Skip shared credential custom field file: ' + iFile.filename);
                        onProgress(progressState);
                        continue;
                    }

                    progressState.filesStage.doneSteps++;
                    progressState.filesStage.messages.push('Re-encrypt custom field file: ' + iFile.filename);
                    onProgress(progressState);

                    // logic to re-encrypt file and replace its credential reference at the end as well as deleting the old file from the server
                    try {
                        newCredentials[i].overwriteVaultKey = undefined;    // set undefined to use the current key to decrypt data
                        let decryptedFileContent = await newCredentials[i].downloadFile(iFile, false);
                        const plainFile: FileInterface = {
                            data: decryptedFileContent,
                            filename: iFile.filename,
                            mimetype: iFile.mimetype,
                            size: iFile.size
                        };
                        newCredentials[i].overwriteVaultKey = newPassword;

                        const uploadResponse = await newCredentials[i].encryptUploadFile(plainFile);
                        if (uploadResponse) {
                            delete plainFile.data;
                            plainFile.file_id = uploadResponse.file_id;
                            plainFile.guid = uploadResponse.guid;
                            plainFile.created = iFile.created;

                            filesToDeleteAfterwardsOnFailure.push(plainFile);
                            filesToDeleteAfterwardsOnSuccess.push(iFile);
                            iCustomFields[j].value = plainFile;
                        } else {
                            // error upload failed
                            onError('An error occurred while uploading a re-encrypted custom field file! - ' + newCredentials[i].label + ": " + iFile.filename);
                            progressState.filesStage.messages.push('Custom field file ' + iFile.file_id + ' upload error');
                            anyFileReEncryptionErrorOccurred = true;
                        }
                    } catch (e) {
                        onError('An error occurred while re-encrypting a custom field file! - ' + newCredentials[i].label + ": " + iFile.filename);
                        progressState.filesStage.messages.push('Custom field file ' + iFile.file_id + ' error:' + e.toString());
                        anyFileReEncryptionErrorOccurred = true;
                    }
                }
                newCredentials[i].custom_fields = iCustomFields;
            }
            progressState.filesStage.doneSteps++;
            progressState.filesStage.messages.push('Done');
            onProgress(progressState);

            progressState.uploadStage = {
                doneSteps: -1,
                totalSteps: 1,
                messages: []
            };
            if (!anyFileReEncryptionErrorOccurred) {
                progressState.uploadStage.totalSteps = newCredentials.length + revisionsToUpdateOnSuccess.length + 3;

                // credential upload
                for (let credential of newCredentials) {
                    progressState.uploadStage.doneSteps++;
                    progressState.uploadStage.messages.push('Upload credential data: ' + credential.label);
                    onProgress(progressState);

                    await credential.update();
                }

                // revisions upload
                for (let revision of revisionsToUpdateOnSuccess) {
                    progressState.uploadStage.doneSteps++;
                    progressState.uploadStage.messages.push('Upload revision data: ' + revision.credential_data.label + ' (revision id: ' + revision.revision_id + ')');
                    onProgress(progressState);

                    await Revision.updateRevision(revision, vault.getServer());
                }

                // update sharing keys
                progressState.uploadStage.doneSteps++;
                progressState.uploadStage.messages.push('Update sharing keys');
                onProgress(progressState);
                const decrypted_private_sharing_key = vault.private_sharing_key;
                vault.vaultKey = newPassword;
                await vault.updateSharingKeys(vault.public_sharing_key, decrypted_private_sharing_key);

                //// re-encrypt ACL sharing keys
                const aclsForCredentialsSharedWithUs = await ShareService.getACLsForCredentialsSharedWithUs(vault, vault.getServer());
                if (aclsForCredentialsSharedWithUs) {
                    for (let acl of aclsForCredentialsSharedWithUs) {
                        try {
                            const decrypted_shared_key = PassmanCrypto.decryptString(acl.shared_key, currentPassword);
                            acl.shared_key = PassmanCrypto.encryptString(decrypted_shared_key, newPassword);
                            await ShareService.updateSharedKeyForCredentialSharedWithUsByGuid(acl.shared_key, acl.item_guid, vault.getServer());
                        } catch (e) {
                            // ignore
                            // can't do anything at this point either
                            console.error(e);
                        }
                    }
                }

                progressState.uploadStage.doneSteps++;
                progressState.uploadStage.messages.push('Refresh vault');
                onProgress(progressState);
                await vault.refresh();
            }

            // delete old / useless files
            progressState.uploadStage.doneSteps++;
            progressState.uploadStage.messages.push('Delete abandoned files from server');
            onProgress(progressState);
            await this.deleteAbandonedFiles(anyFileReEncryptionErrorOccurred, filesToDeleteAfterwardsOnSuccess, filesToDeleteAfterwardsOnFailure, vault);

            progressState.uploadStage.doneSteps++;
            progressState.uploadStage.messages.push('Done');
            onProgress(progressState);
        }
    }

    private static async deleteAbandonedFiles(
        anyFileReEncryptionErrorOccurred: boolean,
        filesToDeleteAfterwardsOnSuccess: FileInterface[],
        filesToDeleteAfterwardsOnFailure: FileInterface[],
        vault: Vault
    ) {
        if (anyFileReEncryptionErrorOccurred) {
            // something was wrong, delete re-encrypted already uploaded files
            for (let file of filesToDeleteAfterwardsOnFailure) {
                await File.deleteFile(file, vault.getServer());
            }
        } else {
            // everything was fine
            for (let file of filesToDeleteAfterwardsOnSuccess) {
                await File.deleteFile(file, vault.getServer());
            }
        }
    }
}
