import forge from 'node-forge';
import sjcl, { SjclCipherEncryptParams } from 'sjcl';
import { GenerateKeypairResponseInterface } from "../Interfaces/PassmanCrypto/GenerateKeypairResponseInterface";
import { RSAKeypairInterface } from "../Interfaces/PassmanCrypto/RSAKeypairInterface";
import { PEMRSAKeypairInterface } from "../Interfaces/PassmanCrypto/PEMRSAKeypairInterface";
import { Base64 } from "js-base64";

export class PassmanCrypto {
    public static generateRSAKeypair = (keyLength: number = 2048): Promise<GenerateKeypairResponseInterface> => {
        return new Promise<GenerateKeypairResponseInterface>(function (resolve, reject) {
            // generate an RSA key pair asynchronously (uses web workers if available)
            // use workers: -1 to run a fast core estimator to optimize # of workers
            forge.pki.rsa.generateKeyPair({
                bits: keyLength,
                workers: 2
            }, (error: Error, keypair: RSAKeypairInterface) => {
                resolve({ error, keypair });
            });
        });
    };

    public static rsaKeyPairToPEM = (keypair: RSAKeypairInterface): PEMRSAKeypairInterface => {
        return {
            privateKey: forge.pki.privateKeyToPem(keypair.privateKey),
            publicKey: forge.pki.publicKeyToPem(keypair.publicKey)
        };
    };

    private static readonly sjcl_encryption_config: SjclCipherEncryptParams = {
        adata: "",
        iter: 1000,
        ks: 256,
        mode: 'ccm',
        ts: 64,
        //salt: [],
        //iv: []
    };

    public static encryptString = (plainText: string, key: string): string => {
        // todo: think about replacing aes-ccm from sjcl with the more modern and faster aes-gcm from forge
        // see https://crypto.stackexchange.com/questions/6842/how-to-choose-between-aes-ccm-and-aes-gcm-for-storage-volume-encryption
        // see https://github.com/digitalbazaar/forge#cipher

        // todo: try to use aes-ccm from jscrypto instead of the very outdated sjcl
        // see https://github.com/Hinaser/jscrypto/blob/master/API.md#aes

        let rp = {};
        const ct = sjcl.encrypt(key, plainText, PassmanCrypto.sjcl_encryption_config, rp);
        return Base64.btoa(ct);
    };

    public static decryptString = (b64EncCiphertext: string, key: string): string => {
        const ciphertext = Base64.atob(b64EncCiphertext);

        try {
            return sjcl.decrypt(key, ciphertext);
        } catch (e) {
            throw e;
        }
    };
}
