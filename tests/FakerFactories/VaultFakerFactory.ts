import { faker } from '@faker-js/faker';
import Vault from "../../lib/Model/Vault";
import { CredentialFakerFactory } from "./CredentialFakerFactory";
import { LoggingHandlerInterface } from "../../lib/Interfaces/LoggingHandlerInterface";
import { NextcloudServerInterface } from "../../lib/Interfaces/NextcloudServer/NextcloudServerInterface";

export class VaultFakerFactory {
    public build = (withFirstCredential = true, customLogger: LoggingHandlerInterface = undefined) => {
        const vault = new Vault(
            {
                challenge_password: "",
                created: Math.trunc(faker.date.recent({ days: 2 }).getTime() / 1000),
                credentials: [],
                delete_request_pending: false,
                guid: faker.string.uuid(),
                last_access: 0,
                name: faker.word.noun(),
                private_sharing_key: "",
                public_sharing_key: "",
                sharing_keys_generated: 0,
                vault_id: faker.number.int(),
                vault_settings: null
            },
            {
                logger: customLogger
            } as NextcloudServerInterface
        );
        vault.vaultKey = faker.internet.password();

        if (withFirstCredential) {
            new CredentialFakerFactory().build(vault, true, {
                label: 'Test key for vault ' + vault.name,
                hidden: true,
                password: 'lorum ipsum'
            }, customLogger);
        }

        return vault;
    }
}
