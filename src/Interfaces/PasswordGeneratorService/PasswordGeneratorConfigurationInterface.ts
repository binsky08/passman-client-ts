export interface PasswordGeneratorConfigurationInterface {
    length: number,
    useUppercase: boolean,
    useLowercase: boolean,
    useDigits: boolean,
    useSpecialChars: boolean,
    avoidAmbiguousCharacters: boolean,
    requireEveryCharType: boolean
}
