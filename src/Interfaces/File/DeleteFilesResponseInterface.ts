export interface DeleteFilesResponseInterface {
    ok: boolean,
    failed: number[]
}
