import { RequestCachingHandlerInterface } from "../Interfaces/RequestCachingHandlerInterface";

export class RequestCachingService implements RequestCachingHandlerInterface {
    constructor(
        private onGetCallback: (key: string) => string,
        private onSetCallback: (key: string, value: string) => void
    ) {
    }

    async set(key: string, value: string): Promise<void> {
        return this.onSetCallback(key, value);
    }

    async get(key: string): Promise<string> {
        return this.onGetCallback(key);
    }
}
