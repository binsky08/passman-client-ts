export interface IconInterface {
    type?: string | boolean,
    content: string
}
