import { FileUploadResponseInterface } from "./FileUploadResponseInterface";

export interface FileDownloadResponseInterface extends FileUploadResponseInterface {
}
