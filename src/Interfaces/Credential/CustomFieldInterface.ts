export interface CustomFieldInterface {
    label: string,
    secret: boolean,
    field_type: string,
    value: string | object | any
}
