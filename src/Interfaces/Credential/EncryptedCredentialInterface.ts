import { IconInterface } from "./IconInterface";
import { ACLInterface } from "../ShareService/ACLInterface";

export interface EncryptedCredentialInterface {
    // plain fields
    credential_id?: number,
    guid?: string,
    user_id: string,
    vault_id: number,
    label: string,

    // encrypted fields
    description: string,
    tags: string,
    email: string,
    username: string,
    password: string,
    url: string,
    files: string,
    custom_fields: string,
    otp: string,
    compromised: string,
    shared_key: string | null

    // plain fields
    favicon: string,        // old, does this field still exist?
    icon: IconInterface | null,
    renew_interval: number | null,
    expire_time: number,
    delete_time: number,
    hidden: boolean,
    created: number,
    changed: number,
    set_share_key: boolean | null,
    skip_revision: boolean | null

    acl?: ACLInterface            // will be injected by the ShareServer for Credentials, shared with us
}
