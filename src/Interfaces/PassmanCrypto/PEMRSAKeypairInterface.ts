export interface PEMRSAKeypairInterface {
    privateKey: string;
    publicKey: string;
}
