import * as OTPAuth from "otpauth";
import { Secret } from "otpauth";
import qrcode from "qrcode-generator";
import qrcodeParser from "qrcode-parser";
import { type OTPAlgorithm, OTPAlgorithms, OTPConfigInterface } from "../Interfaces/Credential/OTPConfigInterface";

export class OTPService {
    public static mergeDefaultOTPConfig = (otp: OTPConfigInterface) => {
        const defaults = {
            algorithm: "SHA1",
            period: 30,
            digits: 6,
        };

        for (const key in defaults) {
            if (otp[key] === undefined || otp[key] == null) {
                otp[key] = defaults[key];
            }
        }
    }

    public static getSecretString = (secret: undefined | string | Secret): string | undefined => {
        if (typeof secret == "object") {
            return secret.utf8;
        }
        return secret;
    }

    public static getDataUrlFromCurrentOTPValues = (otp: OTPConfigInterface): string => {
        const totp = new OTPAuth.TOTP({
            issuer: otp.issuer,
            label: otp.label,
            algorithm: otp.algorithm,
            digits: otp.digits,
            period: otp.period,
            secret: otp.secret
        });
        return decodeURIComponent(totp.toString());
    }

    /**
     * Updates otp.qr_uri with a new QR code and data url based on the raw otp values.
     * @param otp
     */
    public static updateQRFromCurrentOTPValues = (otp: OTPConfigInterface) => {
        if (otp.qr_uri === undefined) {
            otp.qr_uri = {} as any;
        }

        const typeNumberAutoDetect = 0;
        const errorCorrectionLevel = 'L';   // L = 7%
        const qr = qrcode(typeNumberAutoDetect, errorCorrectionLevel);
        otp.qr_uri.qrData = OTPService.getDataUrlFromCurrentOTPValues(otp);
        qr.addData(otp.qr_uri.qrData);
        qr.make();

        const cellSize = 4;
        const padding = 0;
        const canvas = document.createElement("canvas");
        canvas.width = canvas.height = qr.getModuleCount() * cellSize + padding * 2;
        const context = canvas.getContext("2d");
        qr.renderTo2dContext(context, cellSize);

        otp.qr_uri.image = canvas.toDataURL("image/png");
        canvas.remove();
    }

    /**
     * Returns the current token of the given OTP configuration.
     * @param otp
     */
    public static updateOTP = (otp: OTPConfigInterface): string => {
        if (!otp || !otp.secret || otp.secret === "") {
            return;
        }
        if (typeof otp.secret == "string" && otp.secret.includes(' ')) {
            otp.secret = otp.secret.replaceAll(' ', '');
        }

        OTPService.mergeDefaultOTPConfig(otp);

        const totp = new OTPAuth.TOTP({
            issuer: otp.issuer,
            label: otp.label,
            algorithm: otp.algorithm,
            digits: otp.digits,
            period: otp.period,
            secret: otp.secret
        });
        return totp.generate();
    }

    /**
     * Parse input file asynchronous as QR code, extract the TOTP values and return the IOTPConfig, that's built from it.
     * @param input
     */
    public static parseOTPQrCodeFromInputFileData = async (input: string | File | any) => {
        return await qrcodeParser(input).then((otpUrl: string) => {
            const uri = new URL(otpUrl);
            const type = (uri.href.indexOf('totp/') !== -1) ? 'totp' : 'hotp';
            const label = uri.pathname.replace('//' + type + '/', '');
            const otp: OTPConfigInterface = {
                type: type,
                label: decodeURIComponent(label),
                qr_uri: {
                    qrData: decodeURIComponent(otpUrl),
                    image: input
                },
                issuer: uri.searchParams.get('issuer'),
                secret: uri.searchParams.get('secret'),
                algorithm: uri.searchParams.get('algorithm') && OTPAlgorithms.includes(uri.searchParams.get('algorithm') as any)
                    ? uri.searchParams.get('algorithm') as OTPAlgorithm
                    : "SHA1",
                period: uri.searchParams.get('period') ? parseInt(uri.searchParams.get('period')) : 30,
                digits: uri.searchParams.get('digits') ? parseInt(uri.searchParams.get('digits')) : 6,
            };
            return otp;
        });
    }
}
