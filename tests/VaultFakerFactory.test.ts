import { VaultFakerFactory } from "./FakerFactories/VaultFakerFactory";
import { JestLoggingServiceFactory } from "./FakerFactories/JestLoggingServiceFactory";

describe('testing VaultFakerFactory', () => {
    test('test build without first credential', () => {
        const vault = new VaultFakerFactory().build(false);
        expect(vault).not.toBeNull();
        expect(vault.credentials).toHaveLength(0);
    });
    test('test build with first credential', () => {
        const logger = new JestLoggingServiceFactory().build();
        const vault = new VaultFakerFactory().build(true, logger);
        expect(vault).not.toBeNull();
        expect(vault.credentials).toHaveLength(1);

        // test password to be able to decrypt
        expect(vault.credentials[0].password).not.toEqual(undefined);
        expect(logger.onError).not.toHaveBeenCalled();
        expect(logger.anyError).not.toHaveBeenCalled();
    });
});
