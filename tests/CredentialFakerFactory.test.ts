import { VaultFakerFactory } from "./FakerFactories/VaultFakerFactory";
import { CredentialFakerFactory } from "./FakerFactories/CredentialFakerFactory";
import { faker } from "@faker-js/faker";
import { JestLoggingServiceFactory } from "./FakerFactories/JestLoggingServiceFactory";

describe('testing CredentialFakerFactory', () => {
    test('test default build', () => {
        const vault = new VaultFakerFactory().build(false);
        const myTestLabel = faker.company.name() + " t1";
        const myTestMail = "t1" + faker.internet.email();
        const credential = new CredentialFakerFactory().build(vault, false, {
            label: myTestLabel,
            email: myTestMail
        });

        // test getting non encrypted field
        expect(credential.label).toStrictEqual(myTestLabel);

        // test getting encrypted field
        expect(credential.email).toStrictEqual(myTestMail);
    });

    test('test failing field decryption with locked vault', () => {
        const vault = new VaultFakerFactory().build(false);
        const myTestMail = "t1" + faker.internet.email();
        const logger = new JestLoggingServiceFactory().build();
        const credential = new CredentialFakerFactory().build(vault, false, {
            email: myTestMail
        }, logger);

        credential.clearDecryptedDataCache();

        // test getting encrypted field
        expect(credential.email).toStrictEqual(myTestMail);

        vault.lock();
        credential.clearDecryptedDataCache();

        // test getting encrypted field with locked vault
        expect(credential.email).toStrictEqual(undefined);
        expect(logger.onError).toHaveBeenCalledWith(expect.stringContaining('Failed to decrypt field'));
        expect(logger.anyError).toHaveBeenNthCalledWith(1, new TypeError("Cannot read properties of null (reading 'length')"));
        expect(logger.anyError).toHaveBeenNthCalledWith(2, undefined);
    });
});
