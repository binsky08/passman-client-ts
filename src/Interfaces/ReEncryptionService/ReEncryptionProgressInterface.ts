import { ReEncryptionStageProgressInterface } from "./ReEncryptionStageProgressInterface";

export interface ReEncryptionProgressInterface {
    credentialsStage: ReEncryptionStageProgressInterface,
    revisionsStage: ReEncryptionStageProgressInterface,
    filesStage: ReEncryptionStageProgressInterface,
    uploadStage: ReEncryptionStageProgressInterface,
}
