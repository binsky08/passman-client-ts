import type Credential from "../Model/Credential";
import type { ZxcvbnResult } from '@zxcvbn-ts/core';
import { zxcvbn, zxcvbnOptions } from '@zxcvbn-ts/core';
import * as zxcvbnEnPackage from "@zxcvbn-ts/language-en";
import * as zxcvbnCommonPackage from "@zxcvbn-ts/language-common";
import { FilterStatsInterface } from "../Interfaces/CredentialFilterService/FilterStatsInterface";

export enum FILTERS {
    SHOW_ALL,
    COMPROMISED,
    STRENGTH_LOW,
    STRENGTH_MEDIUM,
    STRENGTH_GOOD,
    EXPIRED,
    DELETED,
    ENCRYPTION_BROKEN   // use FILTERS.ENCRYPTION_BROKEN only when you really know what you are doing
}

export class CredentialFilterService {

    public static getFilteredCredentials = (
        allCredentials: Credential[],
        filter: FILTERS,
        additionalFilterText: string = undefined,
        additionalFilterTags: string[] = undefined
    ): Credential[] => {
        let filtered: Credential[] = [];
        for (const credential of allCredentials) {
            if (filter === FILTERS.ENCRYPTION_BROKEN) {
                // do not care about other major categories (like hidden / deleted / text) for this filter option
                if (credential.hasUnspecifiedEncryptionError()) {
                    filtered.push(credential);
                }
                continue;
            }

            if (CredentialFilterService.isHidden(credential)) {
                // hidden credentials are never shown in the list
                // matches usually only the (required) first test credential
                continue;
            }

            const additionalTextFilterEnabled = additionalFilterText !== undefined && additionalFilterText !== '';
            const additionalTagsFilterEnabled = additionalFilterTags !== undefined && additionalFilterTags.length > 0;

            // filter text if possible, otherwise ignore filter and proceed
            if (
                (!additionalTextFilterEnabled || (additionalTextFilterEnabled && CredentialFilterService.matchesFilterText(credential, additionalFilterText))) &&
                (!additionalTagsFilterEnabled || (additionalTagsFilterEnabled && CredentialFilterService.matchesFilterTags(credential, additionalFilterTags)))
            ) {
                if (filter === FILTERS.DELETED) {
                    // deleted credentials will not match any other category
                    if (CredentialFilterService.isDeleted(credential)) {
                        filtered.push(credential);
                    }
                } else {
                    if (CredentialFilterService.isDeleted(credential)) {
                        // deleted credentials will not match any other category
                        continue;
                    }

                    if (filter === FILTERS.SHOW_ALL) {
                        filtered.push(credential);
                    } else if (filter === FILTERS.COMPROMISED) {
                        if (CredentialFilterService.isCompromised(credential)) {
                            filtered.push(credential);
                        }
                    } else if (filter === FILTERS.STRENGTH_LOW) {
                        if (CredentialFilterService.hasLowStrength(credential)) {
                            filtered.push(credential);
                        }
                    } else if (filter === FILTERS.STRENGTH_MEDIUM) {
                        if (CredentialFilterService.hasMediumStrength(credential)) {
                            filtered.push(credential);
                        }
                    } else if (filter === FILTERS.STRENGTH_GOOD) {
                        if (CredentialFilterService.hasGoodStrength(credential)) {
                            filtered.push(credential);
                        }
                    } else if (filter === FILTERS.EXPIRED) {
                        if (CredentialFilterService.isExpired(credential)) {
                            filtered.push(credential);
                        }
                    }
                }
            }
        }
        return filtered;
    }

    public static getFilterStats = (allCredentials: Credential[]): FilterStatsInterface => {
        const stats: FilterStatsInterface = {
            allVisible: 0,
            compromised: 0,
            strengthLow: 0,
            strengthMedium: 0,
            strengthGood: 0,
            expired: 0,
            deleted: 0,
            encryptionBroken: 0
        };
        const options = {
            translations: zxcvbnEnPackage.translations,
            graphs: zxcvbnCommonPackage.adjacencyGraphs,
            dictionary: {},     // do not use dictionaries for a faster evaluation
        };
        zxcvbnOptions.setOptions(options);

        const now = Date.now();
        for (const credential of allCredentials) {
            if (!CredentialFilterService.isHidden(credential)) {
                if (CredentialFilterService.isDeleted(credential)) {
                    stats.deleted++;
                } else {
                    stats.allVisible++;

                    if (CredentialFilterService.isCompromised(credential)) {
                        stats.compromised++;
                    }
                    if (CredentialFilterService.isExpired(credential, now)) {
                        stats.expired++;
                    }

                    if (credential.password != null) {
                        // do not categorize credential by strength if it has no primary password set
                        // zxcvbn will fail for credential.password = null

                        const zxcvbnResult: ZxcvbnResult = zxcvbn(credential.password);
                        if (zxcvbnResult.score >= 3) {
                            stats.strengthGood++;
                        } else if (zxcvbnResult.score == 2) {
                            stats.strengthMedium++;
                        } else {
                            // use strength low also as fallback, if zxcvbnResult is < 0
                            stats.strengthLow++;
                        }
                    }
                }
            }

            // count all types of credentials (also hidden), if its encryption is broken
            if (credential.hasUnspecifiedEncryptionError()) {
                stats.encryptionBroken++;
            }
        }

        return stats;
    }

    public static isHidden(credential: Credential): boolean {
        return credential.hidden;
    }

    public static isDeleted(credential: Credential): boolean {
        return credential.delete_time !== null && credential.delete_time > 0;
    }

    public static isCompromised(credential: Credential): boolean {
        return credential.compromised !== null && credential.compromised != false;
    }

    public static isExpired(credential: Credential, now: number = undefined): boolean {
        if (now === undefined) {
            now = Date.now();
        }
        return credential.expire_time !== 0 && credential.expire_time * 1000 <= now;
    }

    public static hasLowStrength(credential: Credential): boolean {
        if (credential.password == null) {
            // do not categorize credential by strength if it has no primary password set
            return false;
        }
        const zxcvbnResult: ZxcvbnResult = zxcvbn(credential.password);
        return zxcvbnResult.score <= 1;
    }

    public static hasMediumStrength(credential: Credential): boolean {
        if (credential.password == null) {
            // do not categorize credential by strength if it has no primary password set
            return false;
        }
        const zxcvbnResult: ZxcvbnResult = zxcvbn(credential.password);
        return zxcvbnResult.score == 2;
    }

    public static hasGoodStrength(credential: Credential): boolean {
        if (credential.password == null) {
            // do not categorize credential by strength if it has no primary password set
            return false;
        }
        const zxcvbnResult: ZxcvbnResult = zxcvbn(credential.password);
        return zxcvbnResult.score >= 3;
    }

    public static matchesFilterText(credential: Credential, filterText: string): boolean {
        filterText = filterText.toLowerCase();
        if (credential.label != null && credential.label.toLowerCase().indexOf(filterText) > -1) {
            return true;
        } else if (credential.description != null && credential.description.toLowerCase().indexOf(filterText) > -1) {
            return true;
        }
        return false;
    }

    public static matchesFilterTags(credential: Credential, filterTags: string[]): boolean {
        return credential.tags && credential.tags.filter(tag => filterTags.includes(tag.text)).length > 0;
    }
}
