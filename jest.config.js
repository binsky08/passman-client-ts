/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'jsdom',
    transform: {
        // '^.+\\.[tj]sx?$' to process js/ts with `ts-jest`
        // '^.+\\.m?[tj]sx?$' to process js/ts/mjs/mts with `ts-jest`
        '^.+\\.tsx?$': [
            'ts-jest',
            {
                // ts-jest configuration goes here
            },
        ],
    },
    collectCoverage: true,
    coverageReporters: ["html", "text", "text-summary", "cobertura"],
    collectCoverageFrom: ['lib/**/*.js']
};
