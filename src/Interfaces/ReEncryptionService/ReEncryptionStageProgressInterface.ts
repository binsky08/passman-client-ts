export interface ReEncryptionStageProgressInterface {
    doneSteps: number,
    totalSteps: number,
    messages: string[]
}
