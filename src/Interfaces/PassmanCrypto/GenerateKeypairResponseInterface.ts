import { RSAKeypairInterface } from "./RSAKeypairInterface";

export interface GenerateKeypairResponseInterface {
    error: Error,
    keypair: RSAKeypairInterface
}
