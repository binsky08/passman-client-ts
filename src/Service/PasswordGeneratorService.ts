import { CustomMathsService } from "./CustomMathsService";
import { PasswordGeneratorConfigurationInterface } from "../Interfaces/PasswordGeneratorService/PasswordGeneratorConfigurationInterface";

export class PasswordGeneratorService {
    public static getDefaultConfig = (): PasswordGeneratorConfigurationInterface => {
        return {
            length: 16,
            useUppercase: true,
            useLowercase: true,
            useDigits: true,
            useSpecialChars: true,
            avoidAmbiguousCharacters: false,
            requireEveryCharType: true
        };
    }

    public static generate = (configuration: PasswordGeneratorConfigurationInterface) => {
        let generatedPassword = "";

        let characterPool = "";
        let lowercaseCharacters = "abcdefghjkmnpqrstuvwxyz";
        let uppercaseCharacters = "ABCDEFGHJKMNPQRSTUVWXYZ";
        let digits = "23456789";
        let specialCharacters = ".!@#$%^&*";

        if (!configuration.avoidAmbiguousCharacters) {
            lowercaseCharacters += "ilo";
            uppercaseCharacters += "ILO";
            digits += "10";
        }
        if (configuration.useLowercase) {
            characterPool += lowercaseCharacters;
        }
        if (configuration.useUppercase) {
            characterPool += uppercaseCharacters;
        }
        if (configuration.useDigits) {
            characterPool += digits;
        }
        if (configuration.useSpecialChars) {
            characterPool += specialCharacters;
        }

        for (let generatorPosition = 0; generatorPosition < configuration.length; generatorPosition++) {
            let customCharacterPool = characterPool;

            if (configuration.requireEveryCharType) {
                customCharacterPool = "";
                switch (generatorPosition) {
                    case 0:
                        customCharacterPool += lowercaseCharacters;
                        break;
                    case 1:
                        customCharacterPool += uppercaseCharacters;
                        break;
                    case 2:
                        customCharacterPool += digits;
                        break;
                    case 3:
                        customCharacterPool += specialCharacters;
                        break;
                    default:
                        customCharacterPool = characterPool;
                        break;
                }
            }

            generatedPassword += customCharacterPool.charAt(CustomMathsService.getSecureRandomInt(0, customCharacterPool.length - 1));
        }

        return PasswordGeneratorService.shuffle(generatedPassword);
    }

    public static shuffle = (input: string): string => {
        return input.split('').sort(function () {
            return 0.5 - (CustomMathsService.getSecureRandomInt(0, 100) / 100)
        }).join('');
    }
}
