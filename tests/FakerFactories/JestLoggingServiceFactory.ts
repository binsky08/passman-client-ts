import { DefaultLoggingService } from "../../lib/Service/DefaultLoggingService";

export class JestLoggingServiceFactory {
    public build = () => {
        const logger = new DefaultLoggingService();
        logger.onError = jest.fn();
        logger.onSuccess = jest.fn();
        logger.onDebug = jest.fn();
        logger.onThrow = jest.fn();
        logger.onInfo = jest.fn();
        logger.onWarning = jest.fn();
        logger.anyError = jest.fn();
        return logger;
    }
}
