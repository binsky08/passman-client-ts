import Credential from "./Credential";
import { NextcloudServerInterface } from "../Interfaces/NextcloudServer/NextcloudServerInterface";
import type Vault from "./Vault";
import { EncryptedCredentialInterface } from "../Interfaces/Credential/EncryptedCredentialInterface";
import { FileUploadResponseInterface } from "../Interfaces/File/FileUploadResponseInterface";
import { RevisionInterface } from "../Interfaces/Revision/RevisionInterface";
import { Base64 } from "js-base64";

export default class Revision extends Credential {
    public ENCRYPTED_FIELDS = ['description', 'username', 'password', 'files', 'custom_fields', 'otp', 'email', 'tags', 'url', 'compromised'];

    public static updateRevision(revision: RevisionInterface, server: NextcloudServerInterface) {
        const credentialGUID = revision.credential_data.guid;

        revision.credential_data = Base64.btoa(JSON.stringify(revision.credential_data)) as any;
        return server.postJson<FileUploadResponseInterface>(
            '/credentials/' + credentialGUID + '/revision/' + revision.revision_id,
            revision,
            () => {
            },
            'PATCH'
        );
    }

    /**
     * Create a revision object based on its encrypted data.
     * @param data
     * @param vault
     * @param server
     */
    public static async fromData(data: EncryptedCredentialInterface, vault: Vault, server: NextcloudServerInterface) {
        return new Revision(vault, server, data);
    }

    /**
     * Creates a local 100% clone of the current credential.
     */
    public clone(): Revision {
        const newCredential = new Revision(this.vault, this.server, this.encryptedData);
        newCredential.sharedCredentialEncryptionKey = this.sharedCredentialEncryptionKey;

        return newCredential;
    }
}
